﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PZ_06
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            button2.Click += new EventHandler(Klik2);
            button3.Click += new EventHandler(Klik3);
            button4.Click += (( sender,  e) =>
              {
                  int wynik = Convert.ToInt32(textBox5.Text) - Convert.ToInt32(textBox7.Text);
                  label9.Text = wynik.ToString();
              });
            button5.Click += new EventHandler(delegate (object s,EventArgs e){
                int wynik = Convert.ToInt32(textBox4.Text) % Convert.ToInt32(textBox6.Text);
                label10.Text = wynik.ToString();
            });
        }

        private void button4_Click(object sender, EventArgs e)
        {

        }

        private void label9_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            //Console.WriteLine(textBox1.Text +" "+ textBox2.Text);
          int wynik = Convert.ToInt32(textBox1.Text) + Convert.ToInt32(textBox10.Text);
            label6.Text = wynik.ToString();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox10_TextChanged(object sender, EventArgs e)
        {

        }

        public void Klik2(object sender, EventArgs e)
        {
            int wynik = Convert.ToInt32(textBox2.Text) - Convert.ToInt32(textBox9.Text);
            label7.Text = wynik.ToString();
          
        }
        public void Klik3(object sender, EventArgs e)
        {
            int wynik = Convert.ToInt32(textBox3.Text) * Convert.ToInt32(textBox8.Text);
            label8.Text = wynik.ToString();
      
        }
    }
}
